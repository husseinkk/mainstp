<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participanForm', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('workshop1');
            $table->string('workshop2');
            $table->string('workshop3');
            $table->string('faculty');
            $table->string('university');
            $table->string('level');
            $table->string('status');
            $table->string('previous_experience');
            $table->string('why_to_join');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participanForm');
    }
}
